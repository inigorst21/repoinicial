Preparar café forma parte de la rutina diaria de millones de personas.
Sólo en Estados Unidos, millones de personas beben café diariamente.
Si nunca has utilizado una cafetera, el proceso de elaboración puede ser cualquier cosa menos intuitivo.
Sigue estos sencillos pasos para hacerte una buena taza de café utilizando tu mezcla favorita.
